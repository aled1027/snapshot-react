import './App.css';
import snapshot from '@snapshot-labs/snapshot.js';
import { Web3Provider } from '@ethersproject/providers';

function App() {

    const init = async () => {
        const hub = 'https://hub.snapshot.org';
        const client = new snapshot.Client712(hub);

        const web3 = new Web3Provider(window.ethereum);
        const [account] = await web3.listAccounts();

        //const receipt = await client.vote(web3, account, {
        //    space: 'yam.eth',
        //    proposal: '0x21ea31e896ec5b5a49a3653e51e787ee834aaf953263144ab936ed756f36609f',
        //    type: 'single-choice',
        //    choice: 1,
        //    metadata: JSON.stringify({})
        //});

        const receipt = await client.proposal(web3, account, {
            space: 'yam.eth',
            type: 'single-choice',
            title: 'Test proposal using Snapshot.js',
            body: '',
            choices: ['Alice', 'Bob', 'Carol'],
            start: 1636984800,
            end: 1637244000,
            snapshot: 13620822,
            network: 1,
            strategies: JSON.stringify({}),
            plugins: JSON.stringify({}),
            metadata: JSON.stringify({ app: 'snapshot.js' })
        });
    };
    init();


    return (
        <div className="App">
        Hello
        </div>
    );
}

export default App;
